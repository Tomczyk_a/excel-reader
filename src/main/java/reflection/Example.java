package reflection;

@ExampleAnnotation
public class Example {

    private String pole1;
    private Double pole2;
    private Integer pole3;


    public Example(String pole1, Double pole2, Integer pole3) {
        this.pole1 = pole1;
        this.pole2 = pole2;
        this.pole3 = pole3;
    }

    public Example() {
    }


    public String getPole1() {
        return pole1;
    }

    public void setPole1(String pole1) {
        this.pole1 = pole1;
    }

    public Double getPole2() {
        return pole2;
    }

    public void setPole2(Double pole2) {
        this.pole2 = pole2;
    }

    public Integer getPole3() {
        return pole3;
    }

    public void setPole3(Integer pole3) {
        this.pole3 = pole3;
    }

    public void hello() {
        System.out.println("hello world");
    }
}
