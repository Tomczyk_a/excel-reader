package reflection;



import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;



public class ExampleTest {

    @Test
    public void shouldPrintHelloWorldOnConsole(){
        //given

        //when

        //then
//        Example example = new Example();
//        example.hello();

        try {
            final Class<?> aClass = Class.forName("reflection.Example");
            final Method hello = aClass.getMethod("hello");
            final Object o = aClass.newInstance();

            final Annotation[] annotations = o.getClass().getAnnotations();
            for (Annotation annotation : annotations) {
                System.out.println(annotation.toString());
            }

            hello.invoke(o);

            final Field[] fields = o.getClass().getDeclaredFields();
//            for (Field field : fields) {
//
//            }
            Arrays.stream(fields).forEach(field -> {
                try {
                    field.setAccessible(true);
                    System.out.println(field.getName() + " " + field.get(o));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });

        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

}